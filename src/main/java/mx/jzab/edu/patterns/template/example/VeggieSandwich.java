package mx.jzab.edu.patterns.template.example;

/**
 *
 * @author jzab
 */
public class VeggieSandwich extends Sandwich {

  @Override
  protected boolean wantMeat() {
    return false;
  }

  @Override
  protected void addMeat() {
    throw new UnsupportedOperationException( "Not meat in Veggie sandwich." );
  }

  @Override
  protected void addVeggies() {
    super.addVeggies();
    System.out.println( "Add Carrot" );
    System.out.println( "Add Eggplan" );
    System.out.println( "Add Pepper" );
  }

}
