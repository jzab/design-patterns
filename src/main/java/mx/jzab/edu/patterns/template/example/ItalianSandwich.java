package mx.jzab.edu.patterns.template.example;

/**
 *
 * @author jzab
 */
public class ItalianSandwich extends Sandwich {

  public ItalianSandwich() {
    System.out.println( "Italian Sandwich" );
  }


  @Override
  protected void addMeat() {
    System.out.println( "Add Ham" );
    System.out.println( "add Peperoni" );
  }

}
