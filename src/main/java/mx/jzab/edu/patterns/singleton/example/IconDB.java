package mx.jzab.edu.patterns.singleton.example;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author jzab
 */
public class IconDB {

  private static IconDB instance;

  private IconDB() {
  }

  public Icon getIconFor( String key ) {
    return new ImageIcon( "path/for/image/" + key + ".jpg" );
  }

  public static synchronized IconDB getInstance() {
    if( instance == null ) {
      instance = new IconDB();
    }
    return instance;
  }


}
