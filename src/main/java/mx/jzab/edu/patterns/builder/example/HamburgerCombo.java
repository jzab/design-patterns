package mx.jzab.edu.patterns.builder.example;

/**
 *
 * @author jzab
 */
public class HamburgerCombo {

  private String drink;
  private String meat;
  private String topping;
  private String fries;

  public HamburgerCombo( HamburgerComboBuilder builder ) {
    this.drink = builder.getDrink();
    this.meat = builder.getMeat();
    this.topping = builder.getTopping();
    this.fries = builder.getFries();
  }

  /**
   * @return the drink
   */
  public String getDrink() {
    return drink;
  }

  /**
   * @param drink the drink to set
   */
  public void setDrink( String drink ) {
    this.drink = drink;
  }

  /**
   * @return the meat
   */
  public String getMeat() {
    return meat;
  }

  /**
   * @param meat the meat to set
   */
  public void setMeat( String meat ) {
    this.meat = meat;
  }

  /**
   * @return the topping
   */
  public String getTopping() {
    return topping;
  }

  /**
   * @param topping the topping to set
   */
  public void setTopping( String topping ) {
    this.topping = topping;
  }

  /**
   * @return the fries
   */
  public String getFries() {
    return fries;
  }

  /**
   * @param fries the fries to set
   */
  public void setFries( String fries ) {
    this.fries = fries;
  }

  @Override
  public String toString() {
    return "Drink:" + drink + ", Meat:" + meat + ", Topping:" + topping + ", Fries: " + fries;
  }

}
