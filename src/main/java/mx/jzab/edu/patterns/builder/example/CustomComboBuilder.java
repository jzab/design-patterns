package mx.jzab.edu.patterns.builder.example;

/**
 *
 * @author jzab
 */
public class CustomComboBuilder extends HamburgerComboBuilder {

  private String drink;
  private String meat;
  private String topping;
  private String fries;

  /**
   * @return the drink
   */
  public String getDrink() {
    return drink;
  }

  /**
   * @param drink the drink to set
   */
  public CustomComboBuilder setDrink( String drink ) {
    this.drink = drink;
    return this;
  }

  /**
   * @return the meat
   */
  public String getMeat() {
    return meat;
  }

  /**
   * @param meat the meat to set
   */
  public CustomComboBuilder setMeat( String meat ) {
    this.meat = meat;
    return this;
  }

  /**
   * @return the topping
   */
  public String getTopping() {
    return topping;
  }

  /**
   * @param topping the topping to set
   */
  public CustomComboBuilder setTopping( String topping ) {
    this.topping = topping;
    return this;
  }

  /**
   * @return the fries
   */
  public String getFries() {
    return fries;
  }

  /**
   * @param fries the fries to set
   */
  public CustomComboBuilder setFries( String fries ) {
    this.fries = fries;
    return this;
  }

}
