package mx.jzab.edu.patterns.decorator.refactor;

/**
 *
 * @author zjaramil
 */
public interface MessageTransformer {

  public String transform();

}
