package mx.jzab.edu.patterns.decorator.familiar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;

/**
 *
 * @author jzab
 */
public class StreamMain {

  public static void main( String[] args ) throws FileNotFoundException, IOException {
    Reader reader = new FileReader( new File( "" ) );
    reader.read();

    BufferedReader buffer = new BufferedReader( reader );
    buffer.readLine();

    LineNumberReader lineReader = new LineNumberReader( reader );
    lineReader.getLineNumber();

    buffer.readLine();
  }

}
