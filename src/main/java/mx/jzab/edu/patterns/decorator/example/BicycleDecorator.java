package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public abstract class BicycleDecorator implements Bicycle {

  protected Bicycle bike;

   public BicycleDecorator( Bicycle bike ) {
      this.bike = bike;
   }

   @Override
   public int getTopSpeed() {
     return bike.getTopSpeed();
   }

   @Override
   public double getCost() {
     return bike.getCost();
   }

   @Override
   public void doBreak() {
     bike.doBreak();
   }


}
