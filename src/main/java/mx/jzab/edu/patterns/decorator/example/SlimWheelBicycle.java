
package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public class SlimWheelBicycle extends BicycleDecorator {

  public SlimWheelBicycle( Bicycle bike ) {
    super( bike );
  }

  @Override
  public int getTopSpeed() {
    return bike.getTopSpeed() + 10;
  }

  @Override
  public double getCost() {
    return bike.getCost() + 15;
  }


}
