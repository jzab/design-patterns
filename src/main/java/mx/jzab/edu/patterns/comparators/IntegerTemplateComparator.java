package mx.jzab.edu.patterns.comparators;

/**
 *
 * @author jzab
 */
public class IntegerTemplateComparator extends NullTemplateDecorator<Integer> {

  public IntegerTemplateComparator( boolean nullFirst ) {
    super( nullFirst );
  }

  @Override
  public int compareObjects( Integer o1, Integer o2 ) {
    return o1.compareTo( o2 );
  }

}
