package mx.jzab.edu.patterns.pool;

import java.util.Stack;

/**
 *
 * @author jzab
 */
public class DBConnectionPool {

  public Stack<String> stack;

  public DBConnectionPool() {
    stack = new Stack<>();
    System.out.println( "Creating Connections" );
    stack.add( "Connection 1" );
    stack.add( "Connection 2" );
    stack.add( "Connection 3" );
    stack.add( "Connection 4" );
    stack.add( "Connection 5" );
  }

  public String get() throws InterruptedException {
    String conn = null;
    do {
      if( stack.isEmpty() ) {
        System.out.println( Thread.currentThread().getName() + " waiting for connection" );
        Thread.sleep( 1500 );
      }
      else {
        synchronized( this ) {
          conn = stack.pop();
          System.out.println( Thread.currentThread().getName() + " got " + conn );
        }
      }
    }
    while( conn == null );
    return conn;
  }

  public synchronized void release( String connection ) {
    stack.push( connection );
    System.out.println( Thread.currentThread().getName() + " Returns " + connection );
  }


}
