package mx.jzab.edu.patterns.factory.example;

import mx.jzab.edu.patterns.factory.DateRandomGenerator;
import mx.jzab.edu.patterns.factory.MathRandomGenerator;
import mx.jzab.edu.patterns.factory.MixedRandomGenerator;
import mx.jzab.edu.patterns.factory.RandomGenerator;

/**
 *
 * @author jzab
 */
public class RandomGeneratorFactory {

  public static RandomGenerator create( int type ) {
    RandomGenerator random = null;
    switch( type ) {
      case 1:
        random = new DateRandomGenerator();
        break;
      case 2:
        random = new MathRandomGenerator();
        break;
      case 3:
        random = new MixedRandomGenerator( new DateRandomGenerator(), new MathRandomGenerator() );
        break;
      default:
        throw new IllegalArgumentException( "No valid type" );
    }
    return random;
  }

}
