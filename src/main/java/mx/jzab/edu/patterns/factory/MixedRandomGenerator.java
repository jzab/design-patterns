package mx.jzab.edu.patterns.factory;

/**
 *
 * @author jzab
 */
public class MixedRandomGenerator implements RandomGenerator {

  private RandomGenerator gen1;
  private RandomGenerator gen2;

  public MixedRandomGenerator( RandomGenerator gen1, RandomGenerator gen ) {
    this.gen1 = gen1;
    this.gen2 = gen2;
  }

  @Override
  public int nextNumber() {
    return ( gen1.nextNumber() + gen2.nextNumber() ) % 10;
  }

}
