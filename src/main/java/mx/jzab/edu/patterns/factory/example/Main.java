package mx.jzab.edu.patterns.factory.example;

import java.util.Scanner;
import mx.jzab.edu.patterns.factory.RandomGenerator;


/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    while( true ) {

      System.out.println( "Choose Generator" );
      System.out.println( "1.- Date" );
      System.out.println( "2.- Random" );
      System.out.println( "3.- Mixed" );

      Scanner scanner = new Scanner( System.in );
      int generator = scanner.nextInt();

      RandomGenerator random = RandomGeneratorFactory.create( generator );

      int randomNumber = random.nextNumber();
      System.out.println( randomNumber );
      int userNumber = 0;
      do {
        System.out.println( "Guess the number" );
        userNumber = scanner.nextInt();

      }
      while( randomNumber != userNumber );

      System.out.println( "Correct" );
    }

  }

}
