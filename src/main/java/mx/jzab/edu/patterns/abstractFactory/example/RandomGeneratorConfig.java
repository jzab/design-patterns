package mx.jzab.edu.patterns.abstractFactory.example;

/**
 *
 * @author jzab
 */
public interface RandomGeneratorConfig {

  public boolean isPositive();

  public int minNumber();

  public int maxNumber();

}
