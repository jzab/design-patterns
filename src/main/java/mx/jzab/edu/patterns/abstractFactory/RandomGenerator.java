package mx.jzab.edu.patterns.abstractFactory;

import mx.jzab.edu.patterns.abstractFactory.example.RandomGeneratorConfig;

/**
 *
 * @author jzab
 */
public abstract class RandomGenerator {

  public int max;
  public int min;
  public boolean isPositive;

  public RandomGenerator( RandomGeneratorConfig config ) {
    this.max = config.maxNumber();
    this.min = config.minNumber();
    this.isPositive = config.isPositive();
  }

  public int generateNumber() {
    int number = nextNumber();
    number = Math.max( min, number );
    number = Math.min( max, number );
    if( isPositive ) {
      number = Math.abs( max );
    }
    else {
      number = Math.negateExact( number );
    }
    return number;
  }


  public abstract int nextNumber();


}
