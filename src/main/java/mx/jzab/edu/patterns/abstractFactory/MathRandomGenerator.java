package mx.jzab.edu.patterns.abstractFactory;

import mx.jzab.edu.patterns.abstractFactory.example.RandomGeneratorConfig;

/**
 *
 * @author jzab
 */
public class MathRandomGenerator extends RandomGenerator {

  public MathRandomGenerator( RandomGeneratorConfig config ) {
    super( config );
  }

  @Override
  public int nextNumber() {
    return (int) ( Math.random() * 10 );
  }

}
