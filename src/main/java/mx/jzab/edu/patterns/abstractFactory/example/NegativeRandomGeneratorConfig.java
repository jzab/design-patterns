package mx.jzab.edu.patterns.abstractFactory.example;

/**
 *
 * @author jzab
 */
public class NegativeRandomGeneratorConfig implements RandomGeneratorConfig {

  @Override
  public boolean isPositive() {
    return false;
  }

  @Override
  public int minNumber() {
    return -100;
  }

  @Override
  public int maxNumber() {
    return -1;
  }

}
