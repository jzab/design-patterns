package mx.jzab.edu.patterns.strategy.refactor;

/**
 *
 * @author zjaramil
 */
public abstract class Employee {

  private String name;
  private String department;

  public Employee( String name, String department )
  {
    this.name = name;
    this.department = department;
  }

  public String getDepartment()
  {
    return department;
  }

}
