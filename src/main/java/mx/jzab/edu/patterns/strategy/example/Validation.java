package mx.jzab.edu.patterns.strategy.example;

/**
 *
 * @author jzab
 */
public interface Validation {

  public abstract boolean isValid( User user );

}
