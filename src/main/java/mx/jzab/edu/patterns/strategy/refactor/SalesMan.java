package mx.jzab.edu.patterns.strategy.refactor;

/**
 *
 * @author zjaramil
 */
public class SalesMan extends Employee {

  public SalesMan( String name, String department )
  {
    super( name, department );
  }

  double getBaseSalary()
  {
    return 15000;
  }

}
