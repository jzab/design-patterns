package mx.jzab.edu.patterns.strategy.refactor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zjaramil
 */
public class Main {

  public static void main( String[] args )
  {
    Employee engineer = new Engineer( "Martin Fowler", "Engineering" );
    Employee sales = new SalesMan( "Johh", "Sales" );
    Employee ceo = new Ceo( "Larry E.", "CEO" );

    List<Employee> employees = new ArrayList<>( 3 );
    employees.add( engineer );
    employees.add( sales );
    employees.add( ceo );


    System.out.println( new PayrollReport().getPerEmployeeReport( employees ) );
    System.out.println( new PayrollReport().getPerDepartmentReport( employees ) );

  }

}
