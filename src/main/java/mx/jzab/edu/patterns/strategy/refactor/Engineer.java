package mx.jzab.edu.patterns.strategy.refactor;

/**
 *
 * @author zjaramil
 */
public class Engineer extends Employee {

  public Engineer( String name, String department )
  {
    super( name, department );
  }

  double getSalary()
  {
    return 10000;
  }

}
