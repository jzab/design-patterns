package mx.jzab.edu.patterns.strategy.familiar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author jzab
 */
public class Strategy {

   public static void main( String[] args ) {

     List<Double> numbers = new ArrayList<>( 7 );
      for( int i = 0; i < 7; i++ ) {
         numbers.add( Math.random() * 100 );
      }

      System.out.println( "---------------ORIGINAL--------------" );
      numbers.stream().forEach( System.out::println );

      System.out.println( "---------------ASCENDING--------------" );
      Collections.sort( numbers, new AscendingComparator() );
      numbers.stream().forEach( System.out::println );

      System.out.println( "---------------DESCENDING--------------" );
      Collections.sort( numbers, new DescendingComparator() );
      numbers.stream().forEach( System.out::println );

   }

   public static class AscendingComparator implements Comparator<Double> {

      @Override
      public int compare( Double number1, Double number2 ) {
         return number1.compareTo( number2 );
      }

   }

   public static class DescendingComparator implements Comparator<Double> {

      @Override
      public int compare( Double number1, Double number2 ) {
         return number2.compareTo( number1 );
      }

   }

}
