package mx.jzab.edu.patterns.strategy.example;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jzab
 */
public abstract class SecurityDevice {

  private final List<Validation> validations;

  public SecurityDevice( Validation validation ) {
    validations = new ArrayList<>( 1 );
    validations.add( validation );
  }

  public void validate( User user ) {
    boolean isValid = validations.stream().allMatch( v -> v.isValid( user ) );
    if( isValid ) {
      openDoor();
    }
    else {
      sendDenyMessage();
    }
  }

  public void addValidation( Validation v ) {
    validations.add( v );
  }

  protected abstract void openDoor();

  protected abstract void sendDenyMessage();

}
